﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class NeighborhoodDto
    {
        public int NeighborhoodCode { get; set; }
        public string NeighborhoodName { get; set; }
        public int CityCode { get; set; }
    }
}
