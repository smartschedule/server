﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class AgeConstraintDto
    {
        public int Id { get; set; }
        public int TeacherId { get; set; }
        public int AgeCode { get; set; }
        public string AgeDescription { get; set; }
    }
}
