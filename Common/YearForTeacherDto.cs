﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class YearForTeacherDto
    {
        public int Id { get; set; }
        public int TeacherId { get; set; }
        public int Year { get; set; }
        public int OrganizationCode { get; set; }
    }
}
