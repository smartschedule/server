﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class DayConstraintDto
    {
        public int Id { get; set; }
        public int TeacherId { get; set; }
        public int DayCode { get; set; }
        public string DayName { get; set; }
    }
}
