﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class TeacherScheduleDto
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public int TeacherId { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }//שרי!! שימי לב להוספות במודלים!!!!
        public int DayCode { get; set; }
        public string DayName { get; set; }//כנ"ל!!!
    }
}
