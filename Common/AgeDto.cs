﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class AgeDto
    {
        public int AgeCode { get; set; }
        public string AgeDescription { get; set; }
    }
}
