﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class AddressConstraintDto
    {
        public int Id { get; set; }
        public int NeighborhoodCode { get; set; }

        public string NeighborhoodName { get; set; }

        public string CityName { get; set; }
        public int TeacherId { get; set; }

    }
}
