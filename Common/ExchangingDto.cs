﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class ExchangingDto
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public int ClassId { get; set; }
        public System.DateTime SourceDate { get; set; }
        public int TecherId { get; set; }
        public int? SubstituteTeacherId { get; set; }
        public System.DateTime? DestinationDate { get; set; }
    }
}
