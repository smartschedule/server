﻿using BL;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class DataController : ApiController
    {
        // GET: api/Data
        [HttpGet]
        public List<TeacherScheduleDto> GetTeacherSchedule(int year,int teacherId)
        {
            return DataBl.GetTeacherSchedule(year, teacherId);
        }

        // GET: api/Data/5
        public  List<AddressConstraintDto> GetAddressConstraint(int id)
        {
            return DataBl.GetAddressConstraint(id);
        }

        // POST: api/Data
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Data/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Data/5
        public void Delete(int id)
        {
        }
    }
}
