﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BL;
using Common;
using DAL;

namespace API.Controllers
{
    public class ConstraintController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        [HttpGet]
        // GET api/<controller>/5
        public List <AddressConstraintDto> GetAddressConstraint(int teacherId)
        {
            return DataBl.GetAddressConstraint(teacherId);
        }
        [HttpGet]
        // GET api/<controller>/5
        public List<DayConstraintDto> GetDayConstraint(int teacherId)
        {
            List<DayConstraintDto>  g=DataBl.GetDayConstraint(teacherId);
            return g;
        }
        [HttpGet]
        // GET api/<controller>/5
        public List<AgeConstraintDto> GetAgeConstraint(int teacherId)
        {
            return DataBl.GetAgeConstraint(teacherId);
        }
        [HttpGet]
        // GET api/<controller>/5
        public List<GenderConstraintDto> GetGenderConstraint(int teacherId)
        {
            return DataBl.GetGenderConstraint(teacherId);
        }
        [HttpGet]
        // GET api/<controller>/5
        public List<NeighborhoodDto> GetAllNeighborhoods()
        {
            return DataBl.GetNeighborhoodList();
        }
        [HttpGet]
        // GET api/<controller>/5
        public List<GenderDto> GetAllGenders()
        {
            return DataBl.GetGenderList();
        }
        [HttpGet]
        // GET api/<controller>/5
        public List<AgeDto> GetAllAges()
        {
            return DataBl.GetAgeList();
        }
        public List<DayDto> GetAllDays()
        {
            return DataBl.GetDayList();
        }
        // POST api/<controller>
        [HttpPost]
        [Route("api/Constraint/SaveDaysConstraint/")]
        public bool SaveDaysConstraint(string[] dayCode,int teacherId)
        {
            return DataBl.SaveDaysConstraint( teacherId,dayCode);
        }
       // string[] dayCode,int teacherId
        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}