﻿using BL;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class ScheduleController : ApiController
    {
        // GET: api/Schedule
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
        [HttpGet]
        public List<TeacherScheduleDto> GetTeacherSchedule(string year,string teacherId)
        {
            return DataBl.GetTeacherSchedule(int.Parse(year),int.Parse(teacherId));
        }


        // POST: api/Schedule
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Schedule/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Schedule/5
        public void Delete(int id)
        {
        }
    }
}
