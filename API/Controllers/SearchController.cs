﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Common;
using DAL;
using BL;

namespace API.Controllers
{
    public class SearchController : ApiController
    {
        // GET: api/Search
        public IEnumerable<ExchangingDto> Get()
        {
            return DAL_Exchanging.GetExchangingList();
        }

        // GET: api/Search/5
        public List<TeacherDto> GetExschanging(int teacherId, string firstDate, string secondDate)
        {
            return SearchBl.SearchExchanging(teacherId,DateTime.Parse(firstDate), DateTime.Parse(secondDate));
        }

        // POST: api/Search
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Search/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Search/5
        public void Delete(int id)
        {
        }
    }
}
