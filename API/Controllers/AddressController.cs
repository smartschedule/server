﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BL;
using Common;

namespace API.Controllers
{
    public class AddressController : ApiController
    {
        // GET: api/Address
        public List<AddressConstraintDto> GetAddressConstraintList()
        {
            return BL_AddressConstraint.GetAddressConstraintList();
        }

        // GET: api/Address/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Address
        public bool Post(AddressConstraintDto a)
        {
            return BL_AddressConstraint.AddAddressConstraintList(a);
        }

        // PUT: api/Address/5
        public bool Put(AddressConstraintDto a)
        {
            return BL_AddressConstraint.UpdateAddressConstraintList(a);
        }

        // DELETE: api/Address/5
        public bool Delete(int id)
        {
            return BL_AddressConstraint.DeleteAddressConstraintList(id);
        }
    }
}
