﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_GenderConstraint
    {
        public static List<GenderConstraintDto> GetGenderConstraintList()
        {
            List<GenderConstraint> list = new List<GenderConstraint>();
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                list = db.GenderConstraint.ToList();
            }
            return Convert.ConvertToGenderConstraintDtoList(list);
        }

        public static bool AddGenderConstraintList(GenderConstraintDto a)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                db.GenderConstraint.Add(Convert.ConvertToGenderConstraint(a));
                db.SaveChanges();
            }
            return true;
        }
        public static bool DeleteGenderConstraintList(int id)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                GenderConstraint genderConstraint = db.GenderConstraint.First(b => (b.id == id));
                db.GenderConstraint.Remove(genderConstraint);
                db.SaveChanges();
            }
            return true;
        }
        public static bool UpdateGenderConstraintList(GenderConstraintDto g)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                GenderConstraint genderConstraint = db.GenderConstraint.First(b => b.id == g.Id);
                genderConstraint.genderCode = g.GenderCode;
                db.SaveChanges();
            }
            return true;
        }
    }
}
