﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_YearForYearForTeacher
    {
        public static List<YearForTeacherDto> GetYearForTeacherList()
        {
            List<YearForTeacher> list = new List<YearForTeacher>();
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                list = db.YearForTeacher.ToList();
            }
            return Convert.ConvertToYearForTeacherDtoList(list);
        }
    }
}
