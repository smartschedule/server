﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
     public class DAL_Gender
    {
        public static List<GenderDto> GetGenderList()
        {
            List<Gender> list = new List<Gender>();
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                list = db.Gender.ToList();
            }
            return Convert.ConvertToGenderDtoList(list);
        }
    }
}
