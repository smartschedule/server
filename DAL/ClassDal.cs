﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_Class
    {
        public static List<ClassDto> GetClassList()
        {
            List<Class> list = new List<Class>();
            using (SmartScheduleEntities db = new SmartScheduleEntities())
            {
                list = db.Classes.ToList();
            }
            return Convert.ConvertToClassCommonList(list);
        }

        public static bool AddClassList(ClassDto a)
        {
            using (SmartScheduleEntities db = new SmartScheduleEntities())
            {
                db.Classes.Add(Convert.ConvertToClass(a));
                db.SaveChanges();
            }
            return true;
        }
        public static bool DeleteClassList(int id)
        {
            using (SmartScheduleEntities db = new SmartScheduleEntities())
            {
                Class Class = db.Classes.First(b => (b.id == id));
                db.Classes.Remove(Class);
                db.SaveChanges();
            }
            return true;
        }
        public static bool UpdateClassList(ClassDto c)
        {
            using (SmartScheduleEntities db = new SmartScheduleEntities())
            {
                Class class1 = db.Classes.First(b => b.id == c.Id);
                class1.ageCode = c.AgeCode;
                class1.buildingNumber = c.BuildingNumber;
                class1.genderCode = c.GenderCode;
                class1.name = c.Name;
                class1.neighborhoodCode = c.NeighborhoodCode;
                class1.genderCode = c.GenderCode;
                class1.organizationCode = c.OrganizationCode;
                class1.street = c.Street;
                db.SaveChanges();
            }
            return true;
        }
    }
}
