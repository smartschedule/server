﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_Teacher
    {
        public static  List<TeacherDto> GetTeacherList()
        {
            List<Teacher> list = new List<Teacher>();
            using (SmartScheduleEntities db = new SmartScheduleEntities())
            {
                list = db.Teachers.ToList();
            }
            return Convert.ConvertToTeacherCommonList(list);
        }

        public static TeacherDto GetTeacherById(int id)
        {
            Teacher t= new Teacher();
            using (SmartScheduleEntities db = new SmartScheduleEntities())
            {
                t = db.Teachers.ToList().First(a=>a.id == id);
            }
            return Convert.ConvertToTeacherCommon(t);
        }
    }
}
