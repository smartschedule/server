﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_Exchanging
    {
        public static List<ExchangingDto> GetExchangingList()
        {
            List<Exchanging> list = new List<Exchanging>();
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                list = db.Exchanging.ToList();
            }
            return Convert.ConvertToExchangingDtoList(list);
        }

        public static bool AddExchangingList(ExchangingDto a)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                db.Exchanging.Add(Convert.ConvertToExchanging(a));
                db.SaveChanges();
            }
            return true;
        }
        public static bool DeleteExchangingList(int id)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                Exchanging exchanging = db.Exchanging.First(b => (b.id == id));
                db.Exchanging.Remove(exchanging);
                db.SaveChanges();
            }
            return true;
        }
        public static bool UpdateExchangingList(ExchangingDto a)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                Exchanging exchanging = db.Exchanging.First(b => b.id == a.Id);
                exchanging.classId = a.ClassId;
                exchanging.destinationDate = a.DestinationDate;
                exchanging.sourceDate = a.SourceDate;
                exchanging.substituteTeacherId = a.SubstituteTeacherId;
                exchanging.year = a.Year;
                   

                db.SaveChanges();
            }
            return true;
        }
    }
}
