﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_City
    {
        public static List<CityDto> GetCityList()
        {
            List<City> list = new List<City>();
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                list = db.City.ToList();
            }
            return Convert.ConvertToCityDtoList(list);
        }
    }
}
