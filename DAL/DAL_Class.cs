﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_Class
    {
        public static List<ClassDto> GetClassList()
        {
            List<Class> list = new List<Class>();
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                list = db.Class.ToList();
            }
            return Convert.ConvertToClassDtoList(list);
        }

        public static bool AddClassList(ClassDto a)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                db.Class.Add(Convert.ConvertToClass(a));
                db.SaveChanges();
            }
            return true;
        }
        public static bool DeleteClassList(int id)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                Class Class = db.Class.First(b => (b.id == id));
                db.Class.Remove(Class);
                db.SaveChanges();
            }
            return true;
        }
        public static bool UpdateClassList(ClassDto c)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                Class class1 = db.Class.First(b => b.id == c.Id);
                class1.ageCode = c.AgeCode;
                class1.buildingNumber = c.BuildingNumber;
                class1.genderCode = c.GenderCode;
                class1.name = c.Name;
                class1.neighborhoodCode = c.NeighborhoodCode;
                class1.genderCode = c.GenderCode;
                class1.organizationCode = c.OrganizationCode;
                class1.street = c.Street;
                db.SaveChanges();
            }
            return true;
        }
    }
}
