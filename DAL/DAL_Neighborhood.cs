﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_Neighborhood
    {
        public static List<NeighborhoodDto> GetNeighborhoodList()
        {
            List<Neighborhood> list = new List<Neighborhood>();
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                list = db.Neighborhood.ToList();
            }
            return Convert.ConvertToNeighborhoodDtoList(list);
        }
    }
}
