﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_Schedule
    {
        public static List<ScheduleDto> GetScheduleList()
        {
            List<Schedule> list = new List<Schedule>();
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                list = db.Schedule.ToList();
            }
            return Convert.ConvertToScheduleDtoList(list);
        }
    }
}
