﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_AddressConstraint
    {
        public static List<AddressConstraintDto> GetAddressConstraintList()
        {
            List<AddressConstraint> list = new List<AddressConstraint>();
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                list = db.AddressConstraint.ToList();
            }
            return Convert.ConvertToAddressConstraintDtoList(list);
        }

        public static bool AddAddressConstraintList(AddressConstraintDto a)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                db.AddressConstraint.Add(Convert.ConvertToAddressConstraint(a));
            }
            return true;

        }
        public static bool DeleteAddressConstraintList(int id)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                AddressConstraint addressConstraint= db.AddressConstraint.First(b=>(b.id == id));
                db.AddressConstraint.Remove(addressConstraint);
            }
            return true;

        }
        public static bool UpdateAddressConstraintList(AddressConstraintDto a)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                AddressConstraint addressConstraint = db.AddressConstraint.First(b => b.id == a.Id);
                addressConstraint.neighborhoodCode=a.NeighborhoodCode;
                db.SaveChanges();  
            }
            return true;
        }
    }

}
