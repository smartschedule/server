﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_Teacher
    {
        public static List<TeacherDto> GetTeacherList()
        {
            List<Teacher> list = new List<Teacher>();
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                list = db.Teacher.ToList();
            }
            return Convert.ConvertToTeacherDtoList(list);
        }

        public static TeacherDto GetTeacherById(int id)
        {
            Teacher t = new Teacher();
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                t = db.Teacher.ToList().First(a => a.id == id);
            }
            return Convert.ConvertToTeacherDto(t);
        }

        public static TeacherDto GetTeacherByEmail(string email)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                try
                {
                    var t = db.YearForTeacher.Include("Teacher").Include("Organization").First(a => a.Teacher.email == email);
                    if (t != null)
                        return new TeacherDto
                        {
                            IsAuthorized = true,
                            Id = t.Teacher.id,
                            FirstName = t.Teacher.firstName,
                            LastName = t.Teacher.lastName,
                            NeighborhoodCode = t.Teacher.neighborhoodCode,
                            Street = t.Teacher.street,
                            BuildingNumber = t.Teacher.buildingNumber,
                            Phone = t.Teacher.phone,
                            Email = t.Teacher.email,
                            Password = t.Teacher.password,
                            OrganizationName = t.Organization.organizationName,
                            ErrorMessage = "",

                        };
                    return new TeacherDto
                    {
                        IsAuthorized = false,
                        ErrorMessage = "שם משתמש או סיסמה שגויים"
                    };
                }
                catch (Exception ex)
                {
                    return new TeacherDto
                    {
                        IsAuthorized = false,
                        ErrorMessage = "שגיאה בהתחברות לשרת"
                    };

                }
            }
        }

        //public static TeacherDto Login(int teacherName, string password)
        //    {
        //        if (string.IsNullOrEmpty(teacherName.ToString()) || string.IsNullOrEmpty(password))
        //            return new TeacherDto
        //            {
        //                IsAuthorized = false,
        //                ErrorMessage = "לא התקבלו נתונים"
        //            };
        //        using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
        //        {
        //            try
        //            {
        //                var t = db.YearForTeacher.Include("Teacher").Include("Organization").First(x => x.Teacher.id == teacherName && x.Teacher.password == password);
        //                if (t != null)
        //                    return new TeacherDto
        //                    {
        //                        IsAuthorized = true,
        //                        Id = t.id,
        //                        FirstName = t.Teacher.firstName,
        //                        LastName = t.Teacher.lastName,
        //                        NeighborhoodCode = t.Teacher.neighborhoodCode,
        //                        Street = t.Teacher.street,
        //                        BuildingNumber = t.Teacher.buildingNumber,
        //                        Phone = t.Teacher.phone,
        //                        Email = t.Teacher.email,
        //                        Password = t.Teacher.password,
        //                        OrganizationName = t.Organization.organizationName,
        //                        ErrorMessage = "",

        //                    };
        //                return new TeacherDto
        //                {
        //                    IsAuthorized = false,
        //                    ErrorMessage = "שם משתמש או סיסמה שגויים"
        //                };
        //            }
        //            catch (Exception ex)
        //            {
        //                return new TeacherDto
        //                {
        //                    IsAuthorized = false,
        //                    ErrorMessage = "שגיאה בהתחברות לשרת"
        //                };
        //            }
        //        }
        //    }
        }
    }
  

