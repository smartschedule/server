﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_DayConstraint
    {
        public static List<DayConstraintDto> GetDayConstraintList()
        {
            List<DayConstraint> list = new List<DayConstraint>();
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                list = db.DayConstraint.ToList();
            }
            return Convert.ConvertToDayConstraintDtoList(list);
        }

        public static bool AddDayConstraintList(DayConstraintDto a)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                db.DayConstraint.Add(Convert.ConvertToDayConstraint(a));
                db.SaveChanges();
            }
            return true;
        }
        public static bool DeleteDayConstraintList(int id)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                DayConstraint dayConstraint = db.DayConstraint.First(b => (b.id == id));
                db.DayConstraint.Remove(dayConstraint);
                db.SaveChanges();
            }
            return true;
        }
        public static bool UpdateDayConstraintList(DayConstraintDto d)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                DayConstraint dayConstraint = db.DayConstraint.First(b => b.id == d.Id);
                dayConstraint.dayCode = d.DayCode;
                db.SaveChanges();
            }
            return true;
        }

        public static bool SaveDaysConstraint(string[] dayCode, int teacherId)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                for(int i=0;i< dayCode.Length;i++)
                {
                    try
                    {
                        db.DayConstraint.Add(new DayConstraint()
                        {
                            teacherId = teacherId,
                            dayCode =int.Parse(dayCode[i]),
                        });
                    }
                    catch(Exception e)
                    {
                        return false;
                    }
                }
                db.SaveChanges();
                return true;
               
            }
        }
    }
}
