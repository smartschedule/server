﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_AgeConstraint
    {
        public  static List<AgeConstraintDto> GetAgeConstraintList()
        {
            List<AgeConstraint> list = new List<AgeConstraint>();
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                list = db.AgeConstraint.ToList();
            }
            return Convert.ConvertToAgeConstraintDtoList(list);
        }

        public static bool AddAgeConstraintList(AgeConstraintDto a)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                db.AgeConstraint.Add(Convert.ConvertToAgeConstraint(a));
                db.SaveChanges();  
            }
            return true;
        }
        public static bool DeleteAgeConstraintList(int id)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                AgeConstraint ageConstraint = db.AgeConstraint.First(b => (b.id == id));
                db.AgeConstraint.Remove(ageConstraint);
                db.SaveChanges();
            }
            return true;
        }
        public static bool UpdateAgeConstraintList(AgeConstraintDto a)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                AgeConstraint ageConstraint = db.AgeConstraint.First(b => b.id == a.Id);
                ageConstraint.ageCode= a.AgeCode;
                db.SaveChanges(); 
            }
            return true;
        }
    }
}
