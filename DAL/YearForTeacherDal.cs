﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class YearForYearForTeacherDal
    {
        public static List<YearForTeacherDto> GetYearForTeacherList()
        {
            List<YearForTeacher> list = new List<YearForTeacher>();
            using (SmartScheduleEntities db = new SmartScheduleEntities())
            {
                list = db.YearForTeachers.ToList();
            }
            return Convert.ConvertToYearForTeacherDList(list);
        }
    }
}
