﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_AddressConstraint
    {
        public static List<AddressConstraintDto> GetAddressConstraintList()
        {
            List<AddressConstraint> list = new List<AddressConstraint>();
            using (SmartScheduleEntities db = new SmartScheduleEntities())
            {
                list = db.AddressConstraints.ToList();
            }
            return Convert.ConvertToAddressConstraintCommonList(list);
        }

        public static bool AddAddressConstraintList(AddressConstraintDto a)
        {
            using (SmartScheduleEntities db = new SmartScheduleEntities())
            {
                db.AddressConstraints.Add(Convert.ConvertToAddressConstraint(a));
            }
            return true;

        }
        public static bool DeleteAddressConstraintList(int id)
        {
            using (SmartScheduleEntities db = new SmartScheduleEntities())
            {
                AddressConstraint addressConstraint= db.AddressConstraints.First(b=>(b.id == id));
                db.AddressConstraints.Remove(addressConstraint);
            }
            return true;

        }
        public static bool UpdateAddressConstraintList(AddressConstraintDto a)
        {
            using (SmartScheduleEntities db = new SmartScheduleEntities())
            {
                AddressConstraint addressConstraint = db.AddressConstraints.First(b => b.id == a.Id);
                addressConstraint.neighborhoodCode=a.NeighborhoodCode;
                db.SaveChanges();  
            }
            return true;
        }
    }

}
