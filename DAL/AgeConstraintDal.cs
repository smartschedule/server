﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_AgeConstraint
    {
        public  static List<AgeConstraintDto> GetAgeConstraintList()
        {
            List<AgeConstraint> list = new List<AgeConstraint>();
            using (SmartScheduleEntities db = new SmartScheduleEntities())
            {
                list = db.AgeConstraints.ToList();
            }
            return Convert.ConvertToAgeConstraintCommonList(list);
        }

        public static bool AddAgeConstraintList(AgeConstraintDto a)
        {
            using (SmartScheduleEntities db = new SmartScheduleEntities())
            {
                db.AgeConstraints.Add(Convert.ConvertToAgeConstraint(a));
                db.SaveChanges();  
            }
            return true;
        }
        public static bool DeleteAgeConstraintList(int id)
        {
            using (SmartScheduleEntities db = new SmartScheduleEntities())
            {
                AgeConstraint ageConstraint = db.AgeConstraints.First(b => (b.id == id));
                db.AgeConstraints.Remove(ageConstraint);
                db.SaveChanges();
            }
            return true;
        }
        public static bool UpdateAgeConstraintList(AgeConstraintDto a)
        {
            using (SmartScheduleEntities db = new SmartScheduleEntities())
            {
                AgeConstraint ageConstraint = db.AgeConstraints.First(b => b.id == a.Id);
                ageConstraint.ageCode= a.AgeCode;
                db.SaveChanges(); 
            }
            return true;
        }
    }
}
