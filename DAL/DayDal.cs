﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DAL_Day
    {
        public static List<DayDto> GetDayList()
        {
            List<Day> list = new List<Day>();
            using (SmartScheduleEntities db = new SmartScheduleEntities())
            {
                list = db.Days.ToList();
            }
            return Convert.ConvertToDayCommonList(list);
        } 
    }
}
