﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;



namespace DAL
{
    public class DAL_Age
    {
        public static List<AgeDto> GetAgeList()
        {
            List<Age> list = new List<Age>();
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                list = db.Age.ToList();
            }
            return Convert.ConvertToAgeDtoList(list); 
        }
    }

}
