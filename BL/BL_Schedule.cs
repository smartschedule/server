﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BL
{
    class BL_Schedule
    {
        public static List<ScheduleDto> GetScheduleList()
        {
            return DAL_Schedule.GetScheduleList();
        }
        }
}
