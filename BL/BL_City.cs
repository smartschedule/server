﻿using Common;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    class BL_City
    {

        public List<CityDto> GetCityList()
        {
            return DAL_City.GetCityList();
        }
    }
}