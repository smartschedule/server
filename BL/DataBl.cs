﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Common;
using System.Net.Mail;

namespace BL
{
    public class DataBl
    {
        public static List<TeacherScheduleDto> GetTeacherSchedule(int year, int teacherId)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                List<TeacherScheduleDto> list = new List<TeacherScheduleDto>();
                var res = db.Schedule.Include("Day").Include("Class")
                      .Where(x => x.teacherId == teacherId && x.year == year).Select(item => new TeacherScheduleDto
                      {
                          ClassId = item.classId,
                          ClassName = item.Class.name,
                          DayCode = item.dayCode,
                          DayName = item.Day.dayName,
                          Id = item.id,
                          TeacherId = item.teacherId,
                          Year = item.year
                      }).ToList();

                return res;


                //var resLinq = (from s in db.Schedules
                //               join d in db.Days on s.dayCode equals d.dayCode
                //               join c in db.Classes on s.classId equals c.id
                //               where s.year == year && s.teacherId == teacherId
                //               select (new TeacherScheduleDto
                //               {
                //                   ClassId = s.classId,
                //                   ClassName = c.name,
                //                   DayCode = s.dayCode,
                //                   DayName = d.dayName,
                //                   Id = s.id,
                //                   TeacherId = s.teacherId,
                //                   Year = s.year
                //               })).ToList();
            }
        }
        public static List<AddressConstraintDto> GetAddressConstraint(int techerId)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                List<AddressConstraintDto> list = new List<AddressConstraintDto>();
                var res = db.AddressConstraint.Include("Teacher").Include("Neighborhood").Include("City")
                    .Where(x => x.teacherId == techerId).Select(item => new AddressConstraintDto
                    {
                        Id = item.id,
                        NeighborhoodCode = item.neighborhoodCode,
                        NeighborhoodName = item.Neighborhood.neighborhoodName,
                        CityName = item.Neighborhood.City.cityName,
                        TeacherId = item.teacherId
                    }).ToList();
                return res;
            }
        }


        public static List<GenderConstraintDto> GetGenderConstraint(int techerId)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                List<GenderConstraintDto> list = new List<GenderConstraintDto>();
                var res = db.GenderConstraint.Include("Teacher").Include("Gender")
                    .Where(x => x.teacherId == techerId).Select(item => new GenderConstraintDto
                    {
                        Id = item.id,
                        GenderCode = item.genderCode,
                        GenderName = item.Gender.genderDescription,
                        TeacherId = item.teacherId
                    }).ToList();
                return res;
            }
        }
        public static List<AgeConstraintDto> GetAgeConstraint(int techerId)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                List<AgeConstraintDto> list = new List<AgeConstraintDto>();
                var res = db.AgeConstraint.Include("Teacher").Include("Age")
                    .Where(x => x.teacherId == techerId).Select(item => new AgeConstraintDto
                    {
                        Id = item.id,
                        AgeCode = item.ageCode,
                        AgeDescription = item.Age.ageDescription,
                        TeacherId = item.teacherId
                    }).ToList();
                return res;
            }
        }
        public static List<DayConstraintDto> GetDayConstraint(int techerId)
        {
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                List<DayConstraintDto> list = new List<DayConstraintDto>();
                var res = db.DayConstraint.Include("Teacher").Include("Day")
                    .Where(x => x.teacherId == techerId).Select(item => new DayConstraintDto
                    {
                        Id = item.id,
                        DayCode = item.dayCode,
                        DayName = item.Day.dayName,
                        TeacherId = item.teacherId
                    }).ToList();
                return res;
            }
        }
        public static TeacherDto GetTeacherById(int id)
        {
            return DAL_Teacher.GetTeacherById(id);
        }
        public static List<NeighborhoodDto> GetNeighborhoodList()
        {
            return DAL_Neighborhood.GetNeighborhoodList();
        }
        public static List<GenderDto> GetGenderList()
        {
            return DAL_Gender.GetGenderList();
        }
        public static List<AgeDto> GetAgeList()
        {
            return DAL_Age.GetAgeList();
        }
        public static List<DayDto> GetDayList()
        {
            return DAL_Day.GetDayList();
        }
        //////////////////////////////////////////////////////////////////

        public static bool SaveDaysConstraint(int teacherId,string[] dayCode)
        {
            return DAL_DayConstraint.SaveDaysConstraint(dayCode, teacherId);
        }
        public static void SendEmail()//bl
        {
            string _password = "Student@264";
            string _sender = "207558412@mby.co.il";
            SmtpClient client = new SmtpClient("smtp-mail.outlook.com");


            client.Port = 587;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(_sender, _password);
            client.EnableSsl = true;
            client.Credentials = credentials;
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage(_sender, "207558412@mby.co.il");
            message.Subject = "SmartSchedule!!! ";
            message.Body = " try email project";
            client.Send(message);
        }

    }
}
