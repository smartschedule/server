﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Common;


namespace BL
{
    public class BL_Teacher
    {
        public static List<TeacherDto> GetTeacherList()
        {
            return DAL_Teacher.GetTeacherList();
        }
        public static TeacherDto GetTeacherById(int id)
        {
            return DAL_Teacher.GetTeacherById(id);
        }
        //public static TeacherDto GetTeacherById(int teacherId)
        //{
        //    using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
        //    {
        //        TeacherDto teacher = new TeacherDto();
        //        var res = db.YearForTeachers.Include("Teacher").Include("Organization").First(x => x.teacherId == teacherId);
        //        teacher.Id = res.Teacher.id;
        //        teacher.FirstName = res.Teacher.firstName;
        //        teacher.LastName = res.Teacher.lastName;
        //        teacher.NeighborhoodCode = res.Teacher.neighborhoodCode;
        //        teacher.Password = res.Teacher.password;
        //        teacher.Phone = res.Teacher.phone;
        //        teacher.OrganizationName = res.Organization.organizationName;
        //        teacher.Street = res.Teacher.street;

        //        return teacher;
        //    }
        //}
    }
}
