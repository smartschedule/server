﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;

namespace BL
{
    class BL_YearForTeacher
    {
        public static List<YearForTeacherDto> GetYearForTeacherList()
        {
            return DAL_YearForYearForTeacher.GetYearForTeacherList();
        }
    }
}
