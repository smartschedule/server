﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;
namespace BL
{
    class BL_Exchanging
    {

        public List<ExchangingDto> GetExchangingList()
        {
            return DAL_Exchanging.GetExchangingList();
        }
        public static bool AddExchangingList(ExchangingDto a)
        {
            return DAL_Exchanging.AddExchangingList(a);
        }
        public static bool DeleteExchangingList(int id)
        {
            return DAL_Exchanging.DeleteExchangingList(id);
        }
        public static bool UpdateExchangingList(ExchangingDto a)
        {
            return DAL_Exchanging.UpdateExchangingList(a);
        }
    }
}