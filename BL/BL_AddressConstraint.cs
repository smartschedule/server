﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BL
{
    public class BL_AddressConstraint
    {
        public static List<AddressConstraintDto> GetAddressConstraintList()
        {
            return DAL_AddressConstraint.GetAddressConstraintList();
        }
        public static bool AddAddressConstraintList(AddressConstraintDto a)
        {
            return DAL_AddressConstraint.AddAddressConstraintList(a);
        }
        public static bool DeleteAddressConstraintList(int id)
        {
            return DAL_AddressConstraint.DeleteAddressConstraintList(id);
        }
        public static bool UpdateAddressConstraintList(AddressConstraintDto a)
        {
            return DAL_AddressConstraint.UpdateAddressConstraintList(a);
        }
    }
}
