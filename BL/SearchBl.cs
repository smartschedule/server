﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Common;


namespace BL
{
    public class SearchBl
    {
        public static List<TeacherDto> GetTeacherList()
        {
            return DAL_Teacher.GetTeacherList();
        }
        public static TeacherDto GetTeacherById(int id)
        {
            return DAL_Teacher.GetTeacherById(id);
        }
        public static List<ExchangingDto> GetExchangings()
        {
            return DAL_Exchanging.GetExchangingList();
        }

        public static List<TeacherDto> SearchExchanging(int teacherId, DateTime firstDate, DateTime secondDate)
        { //פונקציה לחיפוש מילוי מקום (לא החלפה)
            List<TeacherDto> availableTeachers = new List<TeacherDto>();
            using (SmartScheduleEntities1 db = new SmartScheduleEntities1())
            {
                //שליפת הכיתה בה מבוצעת ההחלפה ע"מ לבדוק את האילוצים
                ClassDto myClass = (from c in db.Class
                               join s in db.Schedule on c.id equals s.classId
                               join t in db.Teacher on s.teacherId equals t.id
                               where firstDate.DayOfWeek == (DayOfWeek)s.dayCode - 1
                               select(new ClassDto
                               {
                                   Id = s.classId,
                                   OrganizationCode = c.organizationCode,
                                   Name = c.name,
                                   NeighborhoodCode = c.neighborhoodCode,
                                   Street = c.street,
                                   BuildingNumber = c.buildingNumber,
                                   AgeCode = c.ageCode,
                                   GenderCode=c.genderCode
                               })).First();
                //שליפת המורות שמתאימות לאילוצים
                var list = (from t in db.Teacher
                            join yearTeacher in db.YearForTeacher on t.id equals yearTeacher.teacherId
                            join organization in db.Organization on yearTeacher.organizationCode equals organization.organizationCode
                            join day in db.DayConstraint on t.id equals day.teacherId
                            join gender in db.GenderConstraint on t.id equals gender.teacherId
                            join address in db.AddressConstraint on t.id equals address.teacherId
                            join age in db.AgeConstraint on t.id equals age.teacherId
                            where myClass.GenderCode == gender.id
                            where myClass.NeighborhoodCode == address.neighborhoodCode
                            where myClass.AgeCode == age.ageCode
                            where firstDate.DayOfWeek == (DayOfWeek)day.dayCode - 1
                            select (new TeacherDto
                                     {
                                         Id = t.id,
                                         FirstName = t.firstName,
                                         LastName = t.lastName,
                                         NeighborhoodCode = t.neighborhoodCode,
                                         Street = t.street,
                                         BuildingNumber = t.buildingNumber,
                                         Phone = t.phone,
                                         Email = t.email,
                                         Password = t.password,
                                         OrganizationName = organization.organizationName,
                                         IsAuthorized = true,
                                         ErrorMessage = ""
                                     })).ToList();
              availableTeachers =list;
            }
            return availableTeachers;
        }

    }
}
