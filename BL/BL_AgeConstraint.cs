﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Common;
namespace BL
{
    class BL_AgeConstraint
    {
        public static List<AgeConstraintDto> GetAgeConstraintList()
        {
            return DAL_AgeConstraint.GetAgeConstraintList();
        }
        public static bool AddAgeConstraintList(AgeConstraintDto a)
        {

            return DAL_AgeConstraint.AddAgeConstraintList(a);
        }
        public static bool DeleteAgeConstraintList(int id)
        {
            return DAL_AgeConstraint.DeleteAgeConstraintList(id);
        }
        public static bool UpdateAgeConstraintList(AgeConstraintDto a)
        {
            return DAL_AgeConstraint.UpdateAgeConstraintList(a);
        }
    }
}
