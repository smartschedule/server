﻿using Common;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    class BL_DayConstraint
    {
        public static List<DayConstraintDto> GetDayConstraintList()
        {
            return DAL_DayConstraint.GetDayConstraintList();
        }
        public static bool AddDayConstraintList(DayConstraintDto a)
        {
            return DAL_DayConstraint.AddDayConstraintList(a);
        }
        public static bool DeleteDayConstraintList(int id)
        {
            return DAL_DayConstraint.DeleteDayConstraintList(id);
        }
        public static bool UpdateDayConstraintList(DayConstraintDto d)
        {
            return DAL_DayConstraint.UpdateDayConstraintList(d);
        }

    }
}