﻿using Common;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    class BL_Class
    {

        public static List<ClassDto> GetClassList()
        {
            return DAL_Class.GetClassList();
        }
        public static bool AddClassList(ClassDto a)
        {
            return DAL_Class.AddClassList(a);
        }
        public static bool DeleteClassList(int id)
        {
            return DAL_Class.DeleteClassList(id);
        }
        public static bool UpdateClassList(ClassDto c)
        {
            return DAL_Class.UpdateClassList(c);
        }
    }
}