﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BL
{
    class BL_GenderConstraint
    {
        public static List<GenderConstraintDto> GetGenderConstraintList()
        {
            return DAL_GenderConstraint.GetGenderConstraintList();
        }
        public static bool AddGenderConstraintList(GenderConstraintDto a)
        {
            return DAL_GenderConstraint.AddGenderConstraintList(a);
        }
        public static bool DeleteDayConstraintList(int id)
        {
            return DAL_GenderConstraint.DeleteGenderConstraintList(id);
        }
        public static bool UpdateGenderConstraintList(GenderConstraintDto g)
        {
            return DAL_GenderConstraint.UpdateGenderConstraintList(g);
        }
    }
}
